package com.perlego;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FixDataService {

	@Autowired
	private FixDataRepository repository;

	public HashMap<String, ArrayList<HashMap<String, Double>>> getTradeCostBySymbol() {
		HashMap<String, ArrayList<HashMap<String, Double>>> symbolMap = new HashMap<>();
		ArrayList<HashMap<String, Double>> accounts;
		for (TradeCost tradeCostSQLRow : repository.getTradeCost()) {
			accounts = symbolMap.get(tradeCostSQLRow.getSymbol());
			HashMap<String, Double> account = new HashMap<>();
			if (accounts == null) {
				accounts = new ArrayList<>();
				account.put(tradeCostSQLRow.getAccount(), tradeCostSQLRow.getTradeCost());
				accounts.add(account);
				symbolMap.put(tradeCostSQLRow.getSymbol(), accounts);
			} else {
				account.put(tradeCostSQLRow.getAccount(), tradeCostSQLRow.getTradeCost());
				accounts.add(account);
			}
		}
		return symbolMap;
	}

	public HashMap<String, ArrayList<HashMap<String, Double>>> getTradeCostByAccount() {
		HashMap<String, ArrayList<HashMap<String, Double>>> accountMap = new HashMap<>();
		ArrayList<HashMap<String, Double>> symbols;
		for (TradeCost tradeCostSQLRow : repository.getTradeCost()) {
			symbols = accountMap.get(tradeCostSQLRow.getAccount());
			HashMap<String, Double> symbol = new HashMap<>();
			if (symbols == null) {
				symbols = new ArrayList<>();
				symbol.put(tradeCostSQLRow.getSymbol(), tradeCostSQLRow.getTradeCost());
				symbols.add(symbol);
				accountMap.put(tradeCostSQLRow.getAccount(), symbols);
			} else {
				symbol.put(tradeCostSQLRow.getSymbol(), tradeCostSQLRow.getTradeCost());
				symbols.add(symbol);
			}
		}
		return accountMap;
	}

	public HashMap<String, Double> getTotalTradeCostBySymbol() {
		HashMap<String, Double> symbolMap = new HashMap<>();
		for (TradeCost tradeCostSQLRow : repository.getTradeCost()) {
			Double totalCost = symbolMap.get(tradeCostSQLRow.getSymbol());
			if (totalCost == null) {
				symbolMap.put(tradeCostSQLRow.getSymbol(), tradeCostSQLRow.getTradeCost());
			} else {
				symbolMap.put(tradeCostSQLRow.getSymbol(), totalCost + tradeCostSQLRow.getTradeCost());
			}
		}
		return symbolMap;
	}

	public HashMap<String, Double> getTotalTradeCostByAccount() {
		HashMap<String, Double> accountMap = new HashMap<>();
		for (TradeCost tradeCostSQLRow : repository.getTradeCost()) {
			Double totalCost = accountMap.get(tradeCostSQLRow.getAccount());
			if (totalCost == null) {
				accountMap.put(tradeCostSQLRow.getAccount(), tradeCostSQLRow.getTradeCost());
			} else {
				accountMap.put(tradeCostSQLRow.getAccount(), totalCost + tradeCostSQLRow.getTradeCost());
			}
		}
		return accountMap;
	}

	public HashMap<String, Double> getIOCHitRateBySymbol() {
		HashMap<String, Double> hitRateMap = new HashMap<>();
		HashMap<String, Double[]> tempMap = new HashMap<>();
		Double[] temp = new Double[] {0.0, 0.0};
		for (IOCHitRate hitRateSQL : repository.getIOCHitRate()) {
			Double[] hitRate = tempMap.get(hitRateSQL.getSymbol());
			if (hitRate == null) {
				double success = hitRateSQL.getSuccess();
				double total = hitRateSQL.getTotal();
				temp[0] = success;
				temp[1] = total;
				tempMap.put(hitRateSQL.getSymbol(), temp);
				hitRateMap.put(hitRateSQL.getSymbol(), success/total);
			} else {
				double success = hitRateSQL.getSuccess() + hitRate[0];
				double total = hitRateSQL.getTotal() + hitRate[1];
				temp[0] = success;
				temp[1] = total;
				tempMap.put(hitRateSQL.getSymbol(), temp);
				hitRateMap.put(hitRateSQL.getSymbol(), success/total);
			}
		}
		return hitRateMap;
	}

	public HashMap<String, Double> getIOCHitRateByAccount() {
		HashMap<String, Double> hitRateMap = new HashMap<>();
		HashMap<String, Double[]> tempMap = new HashMap<>();
		Double[] temp = new Double[] {0.0, 0.0};
		for (IOCHitRate hitRateSQL : repository.getIOCHitRate()) {
			Double[] hitRate = tempMap.get(hitRateSQL.getAccount());
			if (hitRate == null) {
				double success = hitRateSQL.getSuccess();
				double total = hitRateSQL.getTotal();
				temp[0] = success;
				temp[1] = total;
				tempMap.put(hitRateSQL.getAccount(), temp);
				hitRateMap.put(hitRateSQL.getAccount(), success/total);
			} else {
				double success = hitRateSQL.getSuccess() + hitRate[0];
				double total = hitRateSQL.getTotal() + hitRate[1];
				temp[0] = success;
				temp[1] = total;
				tempMap.put(hitRateSQL.getAccount(), temp);
				hitRateMap.put(hitRateSQL.getAccount(), success/total);
			}
		}
		return hitRateMap;
	}

	public HashMap<String, Double> getCommissionBySymbol() {
		HashMap<String, Double> commissionMap = new HashMap<>();
		for (Commission commissionSQL : repository.getCommission()) {
			Double commission = commissionMap.get(commissionSQL.getSymbol());
			if (commission == null) {
				commissionMap.put(commissionSQL.getSymbol(), commissionSQL.getCommission());
			} else {
				commissionMap.put(commissionSQL.getSymbol(), commission + commissionSQL.getCommission());
			}
		}
		return commissionMap;
	}

	public HashMap<String, Double> getCommissionByAccount() {
		HashMap<String, Double> commissionMap = new HashMap<>();
		for (Commission commissionSQL : repository.getCommission()) {
			Double commission = commissionMap.get(commissionSQL.getAccount());
			if (commission == null) {
				commissionMap.put(commissionSQL.getAccount(), commissionSQL.getCommission());
			} else {
				commissionMap.put(commissionSQL.getAccount(), commission + commissionSQL.getCommission());
			}
		}
		return commissionMap;
	}

	public HashMap<String, ArrayList<HashMap<Timestamp, Double>>> getVWAP() {
		HashMap<String, ArrayList<HashMap<Timestamp, Double>>> vwapMap = new HashMap<>();
		ArrayList<HashMap<Timestamp, Double>> vwapArr;
		for (VWAP vwapSQLRow : repository.getVWAP()) {
			vwapArr = vwapMap.get(vwapSQLRow.getSymbol());
			HashMap<Timestamp, Double> vwap = new HashMap<>();
			if (vwapArr == null) {
				vwapArr = new ArrayList<>();
				vwap.put(vwapSQLRow.getSendingTime(), vwapSQLRow.getVwap());
				vwapArr.add(vwap);
				vwapMap.put(vwapSQLRow.getSymbol(), vwapArr);
			} else {
				vwap.put(vwapSQLRow.getSendingTime(), vwapSQLRow.getVwap());
				vwapArr.add(vwap);
			}
		}
		return vwapMap;
	}
}
