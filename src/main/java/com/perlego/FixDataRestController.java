package com.perlego;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin
public class FixDataRestController {

	@Autowired
	private FixDataService service;

	@RequestMapping(method = RequestMethod.GET, value = "/getcommissionbysymbol", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, Double> getCommissionBySymbol(){
		return service.getCommissionBySymbol();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getcommissionbyaccount", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, Double> getCommissionByAccount(){
		return service.getCommissionByAccount();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/gettradecostbysymbol", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, ArrayList<HashMap<String, Double>>> getTradeCostBySymbol(){
		return service.getTradeCostBySymbol();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/gettradecostbyaccount", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, ArrayList<HashMap<String, Double>>> getTradeCostByAccount(){
		return service.getTradeCostByAccount();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/gettotaltradecostbysymbol", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, Double> getTotalTradeCostBySymbol(){
		return service.getTotalTradeCostBySymbol();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/gettotaltradecostbyaccount", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, Double> getTotalTradeCostByAccount(){
		return service.getTotalTradeCostByAccount();
	}
		
	@RequestMapping(method = RequestMethod.GET, value = "/getiochitratebysymbol", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, Double> getIOCHitRateBySymbol(){
		return service.getIOCHitRateBySymbol();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getiochitratebyaccount", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, Double> getIOCHitRateByAccount(){
		return service.getIOCHitRateByAccount();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/getvwap", headers = "Accept=application/json, application/xml, text/plain")
	public HashMap<String, ArrayList<HashMap<Timestamp, Double>>> getVWAP(){
		return service.getVWAP();
	}
}
