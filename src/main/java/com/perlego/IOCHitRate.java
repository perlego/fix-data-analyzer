package com.perlego;

public class IOCHitRate {
	private String account;
	private String symbol;
	private double success;
	private double total;
	
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getSuccess() {
		return success;
	}
	public void setSuccess(double success) {
		this.success = success;
	}
}
