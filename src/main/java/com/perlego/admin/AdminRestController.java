package com.perlego.admin;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/admin")
@CrossOrigin
public class AdminRestController {

	@Autowired
	private AdminService service;

	@RequestMapping(method = RequestMethod.POST, value = "/importfixdata", headers = "Accept=application/json, application/xml, text/plain")
	public void uploadFixData() {
		service.createTable();
		try {
			service.importFixData();
			service.createEngineTables();
		}catch(IOException e) {
			//Pass Error Message to Frontend or something
		}
		
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/generatetable", headers = "Accept=application/json, application/xml, text/plain")
	public void createTablesForEngines() {
		try {
			service.createEngineTables();
		} catch (IOException e) {
			//Pass Error Message to Frontend or something			
		}
	}

}
