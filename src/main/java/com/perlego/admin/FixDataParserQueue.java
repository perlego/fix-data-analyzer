package com.perlego.admin;

import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;

import quickfix.MessageUtils;

public class FixDataParserQueue implements BatchPreparedStatementSetter {
	public static int BATCH_SIZE = 10000;
	public static int NULL_DOUBLE_FIELD = -1;
	private ArrayList<String> messages;
	
	
	public FixDataParserQueue() {
		this.messages = new ArrayList<>(BATCH_SIZE);
	}
	
	@Override
	public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
		String msg = messages.get(i);
		
		String account = MessageUtils.getStringField(msg, 1);
		String ordstatus = MessageUtils.getStringField(msg, 39);
		String symbol = MessageUtils.getStringField(msg, 55);
		Double orderQty = stringToDouble(MessageUtils.getStringField(msg, 38));
		Double lastPx = stringToDouble(MessageUtils.getStringField(msg, 31));
		String timeInForce = MessageUtils.getStringField(msg, 59);
		Timestamp sendingTime = stringToTimestamp(MessageUtils.getStringField(msg, 52));
		String ciordid = MessageUtils.getStringField(msg, 11);
			
		preparedStatement.setString(1, account);
		preparedStatement.setString(2, ordstatus);
		preparedStatement.setString(3, symbol);
		preparedStatement.setDouble(4, orderQty);
		preparedStatement.setDouble(5, lastPx);
		preparedStatement.setString(6, timeInForce);
		preparedStatement.setTimestamp(7, sendingTime);
		preparedStatement.setString(8, ciordid);
	}

	@Override
	public int getBatchSize() {
		return messages.size();
	}
	
	public void addMessage(String msg) {
		messages.add(msg);
	}
	
	private Double stringToDouble(String field) {
		if (field != null) {
			return Double.valueOf(field);
		}
		return (double) NULL_DOUBLE_FIELD;
	}
	

	private Timestamp stringToTimestamp(String stringField) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-hh:mm:ss.SSS");
	    Date parsedDate;
		try {
			parsedDate = dateFormat.parse(stringField);
		    Timestamp timestamp = new Timestamp(parsedDate.getTime());
		    return timestamp;
		} catch (ParseException e) {
			return null;
		}
	}
}
