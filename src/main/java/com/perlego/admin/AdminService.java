package com.perlego.admin;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {

	@Autowired
	AdminRepository repository;
	
	public void createTable() {
		repository.createTable();
	}
	
	public void importFixData() throws IOException{
		repository.importFixData();
	}
	
	public void createEngineTables() throws IOException {
		repository.createPriceQtyTable();
		repository.createIOCTable();
		repository.createVWAPTable();
	}
	
}
 