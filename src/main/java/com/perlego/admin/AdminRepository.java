package com.perlego.admin;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import quickfix.MessageUtils;

@Repository
public class AdminRepository {
	private static String EXECUTION_REPORT = "8";
	
	@Value("${logpath}")
	private String logpath;

	@Autowired
	JdbcTemplate template;
	
	private void batchUpdate(FixDataParserQueue ps) {
		String queryString = "insert into fix (account, ordstatus, symbol, orderqty, lastpx, timeinforce, sendingtime, ciordid) "
				+ "values (?, ?, ?, ?, ?, ?, ?, ?)";
		template.batchUpdate(queryString, ps);
	}
	
	private void addMsgIfIsExecutionOrder(FixDataParserQueue parser, String msg) {
		String msgType = MessageUtils.getStringField(msg, 35);
		if (msgType.equals(EXECUTION_REPORT)) {
			parser.addMessage(msg);
		}
	}
	
	private void runBatchUpdateThread(FixDataParserQueue parser, ExecutorService executorService) {
		FixDataParserQueue clonePs = parser;
		Runnable r = () -> {
			batchUpdate(clonePs);
		};
		executorService.execute(r);
	}
	
	public void createTable() {
		template.execute("drop table if exists fix");
		template.execute("create table fix(id int auto_increment, account varchar(50), ordstatus char(1), "
				+ "symbol varchar(20), orderqty double, lastpx double , timeinforce char(1), "
				+ "sendingtime timestamp, ciordid varchar(50), primary key (id))");
	}

	public void importFixData() throws IOException {
		String msg;
		BufferedReader bufferedReader;
		FixDataParserQueue parser;
		ExecutorService executorService = Executors.newCachedThreadPool();
		try {
			bufferedReader = new BufferedReader(new FileReader(logpath));
			parser = new FixDataParserQueue();
			
			while ((msg = bufferedReader.readLine()) != null) {
				addMsgIfIsExecutionOrder(parser, msg);
				
				if (parser.getBatchSize() == FixDataParserQueue.BATCH_SIZE) {
					runBatchUpdateThread(parser, executorService);
					parser = new FixDataParserQueue();
				}
			}
			batchUpdate(parser);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	
	public void createVWAPTable() throws IOException{
		template.execute("CREATE TABLE vwaptable\r\n" + 
				"  AS (SELECT sum(OrderQty) as qsum,sum(orderqty*LastPx) as pqsum, SendingTime, Symbol \r\n" + 
				"  From fix where OrdStatus='2' group by symbol, sendingtime);");
		template.execute("ALTER TABLE vwaptable \r\n" + 
				" ADD cum_qsum double NULL, \r\n" + 
				" Add cum_pqsum double NULL,\r\n" + 
				" add vwap double null,\r\n" + 
				" CHANGE sendingtime\r\n" + 
				"        sendingtime TIMESTAMP NOT NULL\r\n" + 
				"					DEFAULT CURRENT_TIMESTAMP;");
		String[] symbolList = {"AAPL","CSCO","TSLA"};
		for (String symbol : symbolList) {			
			template.execute("SET SQL_SAFE_UPDATES=0;");
			template.execute("set @cqsum := 0;");
			template.execute("set @cpqsum := 0;");
			template.update( 
					"update vwaptable\r\n" + 
					"set cum_qsum = (@cqsum := @cqsum + qsum),\r\n" + 
					"cum_pqsum = (@cpqsum := @cpqsum + pqsum),\r\n" + 
					"vwap = cum_pqsum/cum_qsum\r\n" + 
					"where Symbol= ? \r\n" + 
					"order by sendingtime;", symbol);
		}
	}
	
	public void createPriceQtyTable() throws IOException{
		template.execute("CREATE TABLE priceQtyTable\r\n" + 
				"  AS (SELECT sum(orderqty*LastPx) as pqsum, account, Symbol \r\n" + 
				"  From fix where OrdStatus='2' group by account, symbol);");
	}
	
	public void createIOCTable() throws IOException{
		template.execute("CREATE TABLE iocTable\r\n" + 
				"  AS (SELECT Account, Symbol, \r\n" + 
				"SUM(case when OrdStatus = '2' then OrderQty else 0 end) as Success, \r\n"+
				"SUM(case when OrdStatus = '0' then OrderQty else 0 end) as Total \r\n" + 
				"FROM fix \r\n" + 
				"WHERE TimeInForce = '3'\r\n" + 
				"GROUP BY Account, Symbol);");
	}
}
