package com.perlego;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


@Repository
public class FixDataRepository {

	@Autowired
	JdbcTemplate template;
	
	public List<Commission> getCommission() throws DataAccessException{
		String sql = "SELECT Account, Symbol, pqsum*0.05 as Commission " + 
				"FROM priceQtyTable " + 
				"GROUP BY Account, Symbol;";
		RowMapper<Commission> rowMapper = (rs, rowNum) -> {
			Commission commission = new Commission();
			commission.setSymbol(rs.getString("Symbol"));
			commission.setAccount(rs.getString("Account"));
			commission.setCommission(rs.getDouble("Commission"));
			return commission;
		};
		return template.query(sql, rowMapper);
	}
	
	public List<TradeCost> getTradeCost() throws DataAccessException{
		String sql = "SELECT Account, Symbol, pqsum*0.02 as TradeCost " + 
				"FROM priceQtyTable " + 
				"GROUP BY Account, Symbol;";
		RowMapper<TradeCost> rowMapper = (rs, rowNum) -> {
			TradeCost tradeCost = new TradeCost();
			tradeCost.setSymbol(rs.getString("Symbol"));
			tradeCost.setAccount(rs.getString("Account"));
			tradeCost.setTradeCost(rs.getDouble("TradeCost"));
			return tradeCost;
		};
		return template.query(sql, rowMapper);
	}
	
	public List<IOCHitRate> getIOCHitRate() throws DataAccessException{
		String sql = "SELECT Account, Symbol, Success, Total\r\n" + 
				"FROM iocTable \r\n" + 
				"GROUP BY Account, Symbol;\r\n";
		RowMapper<IOCHitRate> rowMapper = (rs, rowNum) -> {
			IOCHitRate iocHitRate = new IOCHitRate();
			iocHitRate.setSymbol(rs.getString("Symbol"));
			iocHitRate.setAccount(rs.getString("Account"));
			iocHitRate.setSuccess(rs.getDouble("Success"));
			iocHitRate.setTotal(rs.getDouble("Total"));
			return iocHitRate;
		};
		return template.query(sql, rowMapper);
	}
	
	public List<VWAP> getVWAP() throws DataAccessException{
		String sql = "SELECT Symbol, SendingTime, vwap " + 
				"FROM vwaptable " +  
				"GROUP BY Symbol, SendingTime;";
		RowMapper<VWAP> rowMapper = (rs, rowNum) -> {
			VWAP vwap = new VWAP();
			vwap.setSymbol(rs.getString("Symbol"));
			vwap.setSendingTime(rs.getTimestamp("SendingTime"));
			vwap.setVwap(rs.getDouble("VWAP"));
			return vwap;
		};
		return template.query(sql, rowMapper);
	}
}



