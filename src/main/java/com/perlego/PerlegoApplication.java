package com.perlego;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PerlegoApplication {

	public static void main(String[] args) {
		System.setProperty("spring.config.name", "properties");
		System.setProperty("spring.profiles.active", "production");
		SpringApplication.run(PerlegoApplication.class, args);
	}
}
