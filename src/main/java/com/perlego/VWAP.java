package com.perlego;

import java.sql.Timestamp;

public class VWAP {
	private String symbol;
	private Timestamp sendingTime;
	private double vwap;
	
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	public Timestamp getSendingTime() {
		return sendingTime;
	}
	public void setSendingTime(Timestamp sendingTime) {
		this.sendingTime = sendingTime;
	}
	public double getVwap() {
		return vwap;
	}
	public void setVwap(double vwap) {
		this.vwap = vwap;
	}
}
