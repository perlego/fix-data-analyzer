//package com.perlego.api;
//
//import static org.junit.Assert.*;
//
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("production")
//@TestPropertySource("classpath:properties.yml")
//public class FixDataRestControllerTest {
//	
//	@Autowired
//    private TestRestTemplate restTemplate;
//	
//	@Test
//	public void testGetCommissionBySymbol() {
//		ResponseEntity<HashMap<String, Double>> responseEntity = restTemplate.exchange(
//        		"/api/getcommissionbysymbol",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, Double>>() {});
//
//		HashMap<String, Double> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(3, responseBody.size());
//	}
//
//	@Test
//	public void testGetCommissionByAccount() {
//		ResponseEntity<HashMap<String, Double>> responseEntity = restTemplate.exchange(
//        		"/api/getcommissionbyaccount",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, Double>>() {});
//
//		HashMap<String, Double> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(5, responseBody.size());
//	}
//
//	@Test
//	public void testGetTradeCostBySymbol() {
//		ResponseEntity<HashMap<String, ArrayList<HashMap<String, Double>>>> responseEntity = restTemplate.exchange(
//        		"/api/gettradecostbysymbol",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, ArrayList<HashMap<String, Double>>>>() {});
//
//		HashMap<String, ArrayList<HashMap<String, Double>>> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(3, responseBody.size());
//	}
//
//	@Test
//	public void testGetTradeCostByAccount() {
//		ResponseEntity<HashMap<String, ArrayList<HashMap<String, Double>>>> responseEntity = restTemplate.exchange(
//        		"/api/gettradecostbyaccount",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, ArrayList<HashMap<String, Double>>>>() {});
//
//		HashMap<String, ArrayList<HashMap<String, Double>>> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(5, responseBody.size());
//	}
//
//	@Test
//	public void testGetTotalTradeCostBySymbol() {
//		ResponseEntity<HashMap<String, Double>> responseEntity = restTemplate.exchange(
//        		"/api/gettotaltradecostbysymbol",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, Double>>() {});
//
//		HashMap<String, Double> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(3, responseBody.size());
//	}
//
//	@Test
//	public void testGetTotalTradeCostByAccount() {
//		ResponseEntity<HashMap<String, Double>> responseEntity = restTemplate.exchange(
//        		"/api/gettotaltradecostbyaccount",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, Double>>() {});
//
//		HashMap<String, Double> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(5, responseBody.size());
//	}
//
//	@Test
//	public void testGetIOCHitRateBySymbol() {
//		ResponseEntity<HashMap<String, Double>> responseEntity = restTemplate.exchange(
//        		"/api/getiochitratebysymbol",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, Double>>() {});
//
//		HashMap<String, Double> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(3, responseBody.size());
//	}
//
//	@Test
//	public void testGetIOCHitRateByAccount() {
//		ResponseEntity<HashMap<String, Double>> responseEntity = restTemplate.exchange(
//        		"/api/getiochitratebyaccount",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, Double>>() {});
//
//		HashMap<String, Double> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(5, responseBody.size());
//	}
//
//	@Test
//	public void testGetVWAP() {		
//		ResponseEntity<HashMap<String, ArrayList<HashMap<Timestamp, Double>>>> responseEntity = restTemplate.exchange(
//        		"/api/getvwap",
//        		HttpMethod.GET,
//        		null,
//        		new ParameterizedTypeReference<HashMap<String, ArrayList<HashMap<Timestamp, Double>>>>() {});
//
//		HashMap<String, ArrayList<HashMap<Timestamp, Double>>> responseBody = responseEntity.getBody();
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(5, responseBody.size());
//	}
//
//}
