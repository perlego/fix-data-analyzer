//package com.perlego.api;
//
//import static org.junit.Assert.*;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import com.perlego.FixDataRepository;
//import com.perlego.FixDataService;
//import com.perlego.TradeCost;
//
//@RunWith(SpringRunner.class)
//@ActiveProfiles("development")
//@TestPropertySource("classpath:properties.yml")
//public class FixDataServiceTest {
//	
//	@MockBean
//    private FixDataRepository repositoryMock;
//
//    @Autowired
//    FixDataService service;
//
//	@Test
//	public void testGetTradeCostBySymbol() {
//		List<TradeCost> items = new ArrayList<>();
//    	TradeCost tc1 = new TradeCost();
//    	tc1.setSymbol("APPL");
//    	tc1.setAccount("68B3");
//    	tc1.setTradeCost(29738270);
//    	TradeCost tc2 = new TradeCost();
//    	tc2.setSymbol("CSCO");
//    	tc2.setAccount("78Q1");
//    	tc2.setTradeCost(5714046.0);
//	    when(repositoryMock.getTradeCost()).thenReturn(items);
//        
//		verify(repositoryMock).getTradeCost();
//	}
//
//	@Test
//	public void testGetTradeCostByAccount() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetTotalTradeCostBySymbol() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetTotalTradeCostByAccount() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetIOCHitRateBySymbol() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetIOCHitRateByAccount() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetCommissionBySymbol() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetCommissionByAccount() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testGetVWAP() {
//		fail("Not yet implemented");
//	}
//
//}
