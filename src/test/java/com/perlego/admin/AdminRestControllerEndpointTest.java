//package com.perlego.admin;
//
//import static org.mockito.Mockito.verify;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("development")
//@TestPropertySource("classpath:properties.yml")
//public class AdminRestControllerEndpointTest {
//	@Autowired
//    private TestRestTemplate restTemplate;
//	
//	@MockBean
//	private AdminRestController mockController;
//	
//	@Test
//	public void testUploadFixDataEndpoint() {
//		restTemplate.postForLocation("/admin/importfixdata", null);
//		verify(mockController).uploadFixData();
//	}
//	
//	@Test
//	public void testCreateTablesForEnginesEndpoint() {
//		restTemplate.postForLocation("/admin/generatetable", null);
//		verify(mockController).createTablesForEngines();
//	}
//}
