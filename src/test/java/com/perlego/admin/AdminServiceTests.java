//package com.perlego.admin;
//
//import static org.mockito.Mockito.verify;
//
//import java.io.IOException;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//public class AdminServiceTests {
//	@Autowired
//	AdminService service;
//	
//	@MockBean
//	AdminRepository mockRepo;
//
//	@Test
//	public void testCreateTable() {
//		service.createTable();
//		verify(mockRepo).createTable();
//	}
//
//	@Test
//	public void testImportFixData() throws IOException {
//		service.importFixData();
//		verify(mockRepo).importFixData();
//	}
//
//	@Test
//	public void testCreateEngineTables() throws IOException {
//		service.createEngineTables();
//		verify(mockRepo).createPriceQtyTable();
//		verify(mockRepo).createIOCTable();
//		verify(mockRepo).createVWAPTable();
//	}
//}
