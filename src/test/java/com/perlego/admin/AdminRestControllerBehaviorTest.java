//package com.perlego.admin;
//
//import static org.junit.Assert.*;
//import static org.mockito.Mockito.verify;
//
//import java.io.IOException;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
//@ActiveProfiles("development")
//@TestPropertySource("classpath:properties.yml")
//public class AdminRestControllerBehaviorTest {
//	@Autowired
//	private AdminRestController controller;
//	
//	@MockBean
//	private AdminService mockService;
//	
//	@Test
//	public void testUploadFixDataBehavior() {
//		controller.uploadFixData();
//		verify(mockService).createTable();
//	}
//
//	@Test
//	public void testCreateTablesForEnginesBehavior() throws IOException {
//		controller.createTablesForEngines();
//		verify(mockService).createEngineTables();
//	}
//}
