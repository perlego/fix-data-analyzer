//package com.perlego.admin;
//
//import static org.mockito.Mockito.verify;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.junit4.SpringRunner;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@ActiveProfiles("development")
//@TestPropertySource("classpath:properties.yml")
//public class AdminRepositoryTests {
//	@Autowired
//	private AdminRepository repo;
//	
//	@MockBean 
//	private JdbcTemplate mockTemplate;
//
//	@Test
//	public void testCreateTable() {
//		repo.createTable();
//		verify(mockTemplate).execute("drop table if exists fix");
//		verify(mockTemplate).execute("create table fix(id int auto_increment, account varchar(50), ordstatus char(1), "
//				+ "symbol varchar(20), orderqty double, lastpx double , timeinforce char(1), "
//				+ "sendingtime timestamp, ciordid varchar(50), primary key (id))");
//	}
//}
