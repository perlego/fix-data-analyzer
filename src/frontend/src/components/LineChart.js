import React, {Component} from 'react';
import {Line} from 'react-chartjs-2';
import './Charts.css';

class LineChart extends Component {
    constructor(props) {
      super(props);
      this.state = {
        anchorEl: null,
        filteredElem: 1
      }
    }

    handleClick = (event) => {
      this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = (value) => {
      this.setState({
        anchorEl: null,
        filteredElem: value
      });
    };

    render() {
      const { applTimestamps, applValues, cscoValues, tslaValues } = this.props;
      const dataShown = {
        labels: applTimestamps,
        datasets: [
          {
            label: 'VWap-AAPL',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(255,99,15,0.4)',
            borderColor: 'rgba(255,99,15,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(255,99,15,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(255,99,15,0.6)',
            pointHoverBorderColor: 'rgba(255,99,15,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: applValues
          },
          {
            label: 'VWap-CSCO',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(52,241,23,0.2)',
            borderColor: 'rgba(52,241,23,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(52,241,23,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(52,241,23,0.6)',
            pointHoverBorderColor: 'rgba(52,241,23,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: cscoValues
          },
          {
            label: 'VWap-TSLA',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(23,241,230,0.2)',
            borderColor: 'rgba(23,241,230,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(23,241,230,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(23,241,230,0.6)',
            pointHoverBorderColor: 'rgba(23,241,230,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tslaValues
          },
        ]
      };

      return(
        <div className="chart">
          <Line data={dataShown}
                options={{
                  legend: {
                      labels: {
                          fontColor: "white",
                          fontSize: 10
                      }
                  },
                  scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero: false,
                            callback: function(value, index, values) {
                                return '';
                            },
                        },
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                    }],
                    yAxes: [{
                      ticks: {
                        fontColor: "white",
                        fontSize: 12,
                        beginAtZero: false
                      },
                      gridLines: {
                        color: "#333"
                      }
                    }]
                  },
                }}
          />
          <div className="text-div">
              <span className="text-span">Volume Weighted Average Price Table</span>
          </div>
        </div>
      )
    }
}

export default LineChart;
