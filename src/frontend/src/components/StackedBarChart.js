import React, {Component} from 'react';
import {Bar} from 'react-chartjs-2';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import './Charts.css';

class StackedBarChart extends Component {
    constructor(props) {
      super(props);
      this.state = {
        anchorEl: null,
        filteredElem: 1
      }
    }

    handleClick = (event) => {
      this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = (value) => {
      this.setState({
        anchorEl: null,
        filteredElem: value
      });
    };

    render() {
      const {labels, aaplValues, cscoValues, tslaValues, b3Values, q1Values, pOValues, d2Values, slineValues} = this.props;
      const { anchorEl, filteredElem } = this.state;
      const filterByAccount = {
      	labels: labels,
        datasets: [
          {
            label: 'AAPL',
            data: aaplValues,
            backgroundColor: 'rgba(52,241,23,0.2)', // green
            borderColor: 'rgba(52,241,23,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(52,241,23,0.4)',
            hoverBorderColor: 'rgba(52,241,23,1)'
          },
          {
            label: 'CSCO',
            data: cscoValues,
            backgroundColor: 'rgba(230,241,23,0.2)', // yellow
            borderColor: 'rgba(230,241,23,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(230,241,23,0.4)',
            hoverBorderColor: 'rgba(230,241,23,1)'
          },
          {
            label: 'TSLA',
            data: tslaValues,
            backgroundColor: 'rgba(255,99,132,0.2)', // red
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)'
          },
        ]
      };

      const filterBySymbol = {
        labels: ['APPL', 'CSCO', 'TSLA'],
        datasets: [
          {
            label: '68B3',
            data: b3Values,
            backgroundColor: 'rgba(52,241,23,0.2)', // green
            borderColor: 'rgba(52,241,23,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(52,241,23,0.4)',
            hoverBorderColor: 'rgba(52,241,23,1)'
          },
          {
            label: '78Q1',
            data: q1Values,
            backgroundColor: 'rgba(230,241,23,0.2)', // yellow
            borderColor: 'rgba(230,241,23,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(230,241,23,0.4)',
            hoverBorderColor: 'rgba(230,241,23,1)'
          },
          {
            label: 'C3PO',
            data: pOValues,
            backgroundColor: 'rgba(255,99,132,0.2)', // red
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)'
          },
          {
            label: 'R2D2',
            data: d2Values,
            backgroundColor: 'rgba(23,241,230,0.2)', // blue
            borderColor: 'rgba(23,241,230,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(23,241,230,0.4)',
            hoverBorderColor: 'rgba(23,241,230,1)'
          },
          {
            label: 'SLINE',
            data: slineValues,
            backgroundColor: 'rgba(255,99,15,0.2)', // orange
            borderColor: 'rgba(255,99,15,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,15,0.4)',
            hoverBorderColor: 'rgba(255,99,15,1)'
          },
        ]
      };

      var data = filteredElem === 1 ? filterBySymbol : filterByAccount;
      var title = filteredElem === 1 ? "Individual Trade Cost by Symbol" : "Individual Trade Cost by Account";

      return(
        <div className="chart-bottom-row">
          <div>
            <Button
              aria-owns={anchorEl ? 'simple-menu' : null}
              aria-haspopup="true"
              onClick={this.handleClick}
              style={{color: "white",
                      border: "1px solid grey",
                      fontFamily: "Verdana",
                      fontWeight: "400",
                      fontSize: "10px",
                      float: "right",
                      width: "60px",
                      height: "15px"
                    }}
            >
            Select
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.handleClose}
            >
              <MenuItem onClick={() => this.handleClose(1)}>Symbol</MenuItem>
              <MenuItem onClick={() => this.handleClose(2)}>Account</MenuItem>
            </Menu>
          </div>
          <Bar
            data={data}
            options={{
              maintainAspectRatio: false,
              legend: {
                  labels: {
                      fontColor: "white",
                      fontSize: 10
                  }
              },
              scales: {
                xAxes: [{
                  stacked: true,
                  ticks: {
                      fontColor: "white",
                      fontSize: 14,
                      beginAtZero: true
                  },
                  gridLines: {
                    color: "#333"
                  }
                }],
                yAxes: [{
                  ticks: {
                    fontColor: "white",
                    fontSize: 12,
                    beginAtZero: false
                  },
                  gridLines: {
                    color: "#333"
                  },
                  stacked: true
                }]
              }
            }}
          />
          <div className="text-div">
              <span className="text-span">{title}</span>
          </div>
        </div>
      )
    }
}

export default StackedBarChart;
