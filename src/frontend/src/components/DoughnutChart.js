import React, {Component} from 'react';
import {Doughnut} from 'react-chartjs-2';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import './Charts.css';

class DoughnutChart extends Component {
    constructor(props) {
      super(props);
      this.state = {
        anchorEl: null,
        filteredElem: 1
      }
    }

    handleClick = (event) => {
      this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = (value) => {
      this.setState({
        anchorEl: null,
        filteredElem: value
      });
    };

    render() {
      const {commissionBySymbol, commissionByAccount} = this.props;
      const {filteredElem, anchorEl} = this.state;
      const symbolData = {
      	labels: Object.keys(commissionBySymbol),
      	datasets: [{
      		data: Object.values(commissionBySymbol),
      		backgroundColor: [

            'rgba(255,99,132,0.2)', //red
            'rgba(52,241,23,0.2)', //green
            'rgba(23,241,230,0.2)', //blue
            'rgba(255,99,15,0.2)', //orange
            'rgba(230,241,23,0.2)', //yellow

      		],
      		hoverBackgroundColor: [
            'rgba(255,99,132,0.6)', //red
            'rgba(52,241,23,0.6)', //green
            'rgba(23,241,230,0.6)', //blue
            'rgba(255,99,15,0.6)', //orange
            'rgba(230,241,23,0.6)', //yellow
      		],
          borderColor: [
        		'rgba(255,99,132,1)', //red
            'rgba(52,241,23,1)', //green
            'rgba(23,241,230,1)', //blue
            'rgba(255,99,15,1)', //orange
            'rgba(230,241,23,1)', //yellow


          ]
      	}]
      };

      const accountData = {
        labels: Object.keys(commissionByAccount),
        datasets: [{
          data: Object.values(commissionByAccount),
          backgroundColor: [

            'rgba(255,99,132,0.2)', //red
            'rgba(52,241,23,0.2)', //green
            'rgba(23,241,230,0.2)', //blue
            'rgba(255,99,15,0.2)', //orange
            'rgba(230,241,23,0.2)', //yellow

      		],
      		hoverBackgroundColor: [
            'rgba(255,99,132,0.6)', //red
            'rgba(52,241,23,0.6)', //green
            'rgba(23,241,230,0.6)', //blue
            'rgba(255,99,15,0.6)', //orange
            'rgba(230,241,23,0.6)', //yellow
      		],
          borderColor: [
        		'rgba(255,99,132,1)', //red
            'rgba(52,241,23,1)', //green
            'rgba(23,241,230,1)', //blue
            'rgba(255,99,15,1)', //orange
            'rgba(230,241,23,1)', //yellow


          ]
        }]
      };

      var selectedData = () => {
        if (filteredElem === 1) {
          return symbolData;
        } else {
          return accountData;
        }
      }

      var title = () => {
        if (filteredElem === 1) {
          return "Commission by Symbol";
        } else {
          return "Commission by Account"
        }
      }
      return(
        <div className="chart-bottom-row">
          <div>
            <Button
              aria-owns={anchorEl ? 'simple-menu' : null}
              aria-haspopup="true"
              onClick={this.handleClick}
              style={{color: "white",
                      border: "1px solid grey",
                      fontFamily: "Verdana",
                      fontWeight: "400",
                      fontSize: "10px",
                      float: "right",
                      width: "60px",
                      height: "15px"
                    }}
            >
            Select
            </Button>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={this.handleClose}
            >
              <MenuItem onClick={() => this.handleClose(1)}>Symbol</MenuItem>
              <MenuItem onClick={() => this.handleClose(2)}>Account</MenuItem>
            </Menu>
          </div>
          <Doughnut data={selectedData()}
                    options={{
                      legend: {
                          labels: {
                              fontColor: "white",
                              fontSize: 10
                          }
                      },
                      maintainAspectRatio:false
                    }}
          />
          <div className="text-div">
              <span className="text-span">{title()}</span>
          </div>
        </div>
      )
    }
}

export default DoughnutChart;
