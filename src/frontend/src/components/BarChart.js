import React, {Component} from 'react';
import {Bar} from 'react-chartjs-2';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import './Charts.css';

class BarChart extends Component {
    constructor(props) {
      super(props);
      this.state = {
        anchorEl: null,
        filteredElem: 1
      }
    }

    handleClick = (event) => {
      this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = (value) => {
      this.setState({
        anchorEl: null,
        filteredElem: value
      });
    };

    render() {
      const {totalTradeCostBySymbol, totalTradeCostByAccount, iocHitRateBySymbol, iocHitRateByAccount, tradeCostBySymbol} = this.props;
      const {filteredElem, anchorEl} = this.state;

      var selectedData = () => {
        if (filteredElem === 1) {
          return totalTradeCostBySymbol;
        } else if (filteredElem === 2) {
          return totalTradeCostByAccount;
        } else if (filteredElem === 3) {
          return iocHitRateBySymbol;
        } else if (filteredElem === 4){
          return iocHitRateByAccount;
        } else {
          return tradeCostBySymbol;
        }
      }

      var label = () => {
        if (filteredElem === 1) {
          return "Total Trade Cost By Symbol";
        } else if (filteredElem === 2) {
          return "Total Trade Cost By Account";
        } else if (filteredElem === 3) {
          return "IOC Hit Rate By Symbol";
        } else if (filteredElem === 4 ){
          return "IOC Hit Rate By Account";
        } else {
          return "Individual Trade Cost By Symbol"
        }
      }

      const data = {
        labels: Object.keys(selectedData()),
        datasets: [
          {
            label: label(),
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: Object.values(selectedData())
          }
        ]
      };
      return(
        <div className="chart">
            <div>
              <Button
                aria-owns={anchorEl ? 'simple-menu' : null}
                aria-haspopup="true"
                onClick={this.handleClick}
                style={{color: "white",
                        border: "1px solid grey",
                        fontFamily: "Verdana",
                        fontWeight: "400",
                        fontSize: "10px",
                        float: "right",
                        width: "60px",
                        height: "15px"
                      }}
              >
              Select
              </Button>
              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleClose}
              >
                <MenuItem onClick={() => this.handleClose(1)}>Total Trade Cost By Symbol</MenuItem>
                <MenuItem onClick={() => this.handleClose(2)}>Total Trade Cost By Account</MenuItem>
                <MenuItem onClick={() => this.handleClose(3)}>IOC Hit Rate By Symbol</MenuItem>
                <MenuItem onClick={() => this.handleClose(4)}>IOC Hit Rate by Account</MenuItem>
                <MenuItem onClick={() => this.handleClose(5)}>Individual Trade Cost By Symbol</MenuItem>
              </Menu>
            </div>
            <Bar
              data={data}
              options={{
                maintainAspectRatio: false,
                legend: {
                    labels: {
                        fontColor: "white",
                        fontSize: 10
                    }
                },
                scales: {
                  xAxes: [{
                      ticks: {
                        fontColor: "white",
                        fontSize: 14,
                        beginAtZero: true
                      },
                      gridLines: {
                          color: "#333"
                      },
                  }],
                  yAxes: [{
                    ticks: {
                      fontColor: "white",
                      fontSize: 12,
                      beginAtZero: false
                    },
                    gridLines: {
                      color: "#333"
                    }
                  }]
                },
              }}
            />
            <div className="text-div">
                <span className="text-span">{label()}</span>
            </div>
        </div>
      )
    }
}

export default BarChart;
