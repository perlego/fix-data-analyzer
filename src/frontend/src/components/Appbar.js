import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import LogOutIcon from '@material-ui/icons/Lock';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Utils from './../scripts/utils';
import './Appbar.css';

class Appbar extends Component{
    render () {
        
        return (
            <AppBar className='app-bar' >
                <Toolbar>
                <IconButton color='inherit' aria-label='Menu' onClick = {() => Utils.toURL('/')}>
                    <LogOutIcon />
                </IconButton>
                <Typography variant='title' color='inherit'>
                    Post Trade Analyser
                </Typography>
                </Toolbar>
            </AppBar>
        );
    };
}

export default Appbar;