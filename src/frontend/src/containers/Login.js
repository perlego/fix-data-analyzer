import React, { Component } from 'react';
import Appbar from '../components/Appbar';
import './Login.css'
import { Button } from '@material-ui/core';
import Utils from '../scripts/utils';

class Login extends Component{
    render(){
        return(
            <div>
                
                <div className="login-page">
                </div>

                <Appbar position='static'/>

                <Button id="login-button" onClick = {() => Utils.toURL("/dashboard")}>
                    Enter Dashboard     
                </Button>

            </div>
        );
    }

}

export default Login;