import React, { Component } from 'react';
import Appbar from '../components/Appbar';
import './Dashboard.css';

import BarChart from '../components/BarChart.js';
import LineChart from '../components/LineChart.js';
import DoughnutChart from '../components/DoughnutChart.js';
import StackedBarChart from '../components/StackedBarChart.js';

class Dashboard extends Component{
    constructor() {
      super();
      this.state = {
        tradeCostBySymbol: {},
        tradeCostByAccount: {},
        stackBarAaplValues: [],
        stackBarCscolValues: [],
        stackBarTslalValues: [],
        stackBarLabels: [],
        totalTradeCostBySymbol: {},
        totalTradeCostByAccount: {},
        vwapApplTimestamps: [],
        vwapApplValues: [],
        vwapCscoTimestamps: [],
        vwapCscoValues: [],
        vwapTslaTimestamps: [],
        vwapTslaValues: [],
        commissionBySymbol: {},
        commissionByAccount: {},
        iocHitRateBySymbol: {},
        iocHitRateByAccount: {},
      }
    }

    getTradeCostBySymbol() {
      fetch('http://sg6.prodappserver.conygre.com/api/gettotaltradecostbysymbol')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              tradeCostBySymbol: result
            });
          },
          (error) => {
            console.log("getTradeCostBySymbol failed");
          }
        )
    }

    getTradeCostByAccount() {
      fetch('http://sg6.prodappserver.conygre.com/api/gettradecostbyaccount')
        .then(res => res.json())
        .then(
          (result) => {
            var b3 = result["68B3"];
            var q1 = result["78Q1"];
            var pO = result["C3PO"];
            var d2 = result["R2D2"];
            var sl = result["SLINE"];

            var b3Values = [b3[0]["AAPL"], b3[1]["CSCO"], b3[2]["TSLA"]];
            var q1Values = [q1[0]["AAPL"], q1[1]["CSCO"], q1[2]["TSLA"]];
            var pOValues = [pO[0]["AAPL"], pO[1]["CSCO"], pO[2]["TSLA"]];
            var d2Values = [d2[0]["AAPL"], d2[1]["CSCO"], d2[2]["TSLA"]];
            var slineValues = [sl[0]["AAPL"], sl[1]["CSCO"], sl[2]["TSLA"]];

            this.setState({
              tradeCostByAccount: result,
              stackBarAaplValues: Object.values(result).map(x => x[0]['AAPL']),
              stackBarCscolValues: Object.values(result).map(x => x[1]['CSCO']),
              stackBarTslalValues: Object.values(result).map(x => x[2]['TSLA']),
              stackBarLabels: Object.keys(result),
              b3Values: b3Values,
              q1Values: q1Values,
              pOValues: pOValues,
              d2Values: d2Values,
              slineValues: slineValues
            });
          },
          (error) => {
            console.log("getTradeCostByAccount failed");
          }
        )
    }

    getTotalTradeCostBySymbol() {
      fetch('http://sg6.prodappserver.conygre.com/api/gettotaltradecostbysymbol')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              totalTradeCostBySymbol: result
            });
          },
          (error) => {
            console.log("getTotalTradeCostBySymbol failed");
          }
        )
    }

    getTotalTradeCostByAccount() {
      fetch('http://sg6.prodappserver.conygre.com/api/gettotaltradecostbyaccount')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              totalTradeCostByAccount: result
            });
          },
          (error) => {
            console.log("getTotalTradeCostByAccount failed");
          }
        )
    }

    getVwap() {
      fetch('http://sg6.prodappserver.conygre.com/api/getvwap')
        .then(res => res.json())
        .then(
          (result) => {
            var apple = result['AAPL'];
            var appleVwapTimestampsTmp = apple.map(x => Object.keys(x));
            var appleVwapTimestamps = [].concat.apply([],appleVwapTimestampsTmp);
            var appleVwapValuesTmp = apple.map(x => Object.values(x));
            var appleVWapValues = [].concat.apply([],appleVwapValuesTmp);

            var csco = result['CSCO'];
            var cscoVwapTimestampsTmp = csco.map(x => Object.keys(x));
            var cscoVwapTimestamps = [].concat.apply([],cscoVwapTimestampsTmp);
            var cscoWwapValuesTmp = csco.map(x => Object.values(x));
            var cscoWwapValues = [].concat.apply([],cscoWwapValuesTmp);

            var tsla = result['TSLA'];
            var tslaVwapTimestampsTmp = tsla.map(x => Object.keys(x));
            var tslaVwapTimestamps = [].concat.apply([],tslaVwapTimestampsTmp);
            var tslaVwapValuesTmp = tsla.map(x => Object.values(x));
            var tslaVwapValues = [].concat.apply([],tslaVwapValuesTmp);
            this.setState({
              vwapApplTimestamps: appleVwapTimestamps,
              vwapApplValues: appleVWapValues,
              vwapCscoTimestamps: cscoVwapTimestamps,
              vwapCscoValues: cscoWwapValues,
              vwapTslaTimestamps: tslaVwapTimestamps,
              vwapTslaValues: tslaVwapValues
            });
          },
          (error) => {
            console.log("getVwap failed");
          }
        )
    }

    getCommissionBySymbol() {
      fetch('http://sg6.prodappserver.conygre.com/api/getcommissionbysymbol')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              commissionBySymbol: result
            });
          },
          (error) => {
            console.log("getCommissionBySymbol failed");
          }
        )
    }

    getCommissionByAccount() {
      fetch('http://sg6.prodappserver.conygre.com/api/getcommissionbyaccount')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              commissionByAccount: result
            });
          },
          (error) => {
            console.log("getCommissionByAccount failed");
          }
        )
    }

    getIocHitRateBySymbol() {
      fetch('http://sg6.prodappserver.conygre.com/api/getiochitratebysymbol')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              iocHitRateBySymbol: result
            });
          },
          (error) => {
            console.log("getIocHitRateBySymbol failed");
          }
        )
    }

    getIocHitRateByAccount() {
      fetch('http://sg6.prodappserver.conygre.com/api/getiochitratebyaccount')
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              iocHitRateByAccount: result
            });
          },
          (error) => {
            console.log("getIocHitRateByAccount failed");
          }
        )
    }

    componentWillMount() {
      this.getTradeCostBySymbol();
      this.getTradeCostByAccount();
      this.getTotalTradeCostBySymbol();
      this.getTotalTradeCostByAccount();
      this.getVwap();
      this.getCommissionBySymbol();
      this.getCommissionByAccount();
      this.getIocHitRateBySymbol();
      this.getIocHitRateByAccount();
    }

    componentDidMount(){
      document.title = "Perlego"
    }

    render (){
        return(
            <div className="dash-main-div">
                <table className="maincharttable">
                  <tr className="rowtop">
                    <td className="rowtop">
                      <BarChart totalTradeCostBySymbol={this.state.totalTradeCostBySymbol}
                                totalTradeCostByAccount={this.state.totalTradeCostByAccount}
                                tradeCostBySymbol={this.state.totalTradeCostBySymbol}
                                iocHitRateBySymbol={this.state.iocHitRateBySymbol}
                                iocHitRateByAccount={this.state.iocHitRateByAccount}
                      />
                      </td>
                    <td className="rowtop">
                      <LineChart applTimestamps={this.state.vwapApplTimestamps}
                                 applValues={this.state.vwapApplValues}
                                 cscoTimestamps={this.state.vwapCscoTimestamps}
                                 cscoValues={this.state.vwapCscoValues}
                                 tslaTimestamps={this.state.vwapTslaTimestamps}
                                 tslaValues={this.state.vwapTslaValues}
                      />
                    </td>
                  </tr>
                  <tr className="rowbtm">
                    <td className="rowbtm">
                      <StackedBarChart labels={this.state.stackBarLabels}
                                       aaplValues={this.state.stackBarAaplValues}
                                       cscoValues={this.state.stackBarCscolValues}
                                       tslaValues={this.state.stackBarTslalValues}
                                       b3Values={this.state.b3Values}
                                       q1Values={this.state.q1Values}
                                       pOValues={this.state.pOValues}
                                       d2Values={this.state.d2Values}
                                       slineValues={this.state.slineValues}
                      />
                    </td>
                    <td className="rowbtm">
                      <DoughnutChart commissionBySymbol={this.state.commissionBySymbol}
                                     commissionByAccount={this.state.commissionByAccount}
                      />
                    </td>
                  </tr>
                </table>

            </div>
        );

    }
}

export default Dashboard;
