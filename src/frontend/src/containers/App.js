import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import '../App.css';
import Dashboard from './Dashboard';
import Login from './Login';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Route exact={true} path='/' component={Login} />
        <Route exact={true} path='/dashboard' component={Dashboard} />
      </div>
    );
  }
}

export default App;
